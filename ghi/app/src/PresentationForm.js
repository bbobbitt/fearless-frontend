import React, { useEffect, useState } from 'react';

function PresentationForm() {
    const [presenterName, setPresenterName] = useState('');
    const [presenterEmail, setPresenterEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [title, setTitle] = useState('');
    const [conference, setConference] = useState('');
    const [conferences, setConferences] = useState([]);

    const handleSubmit = async (event) => {
        event.preventDefault();
      
        const data = {};
        data.presenter_name = presenterName;
        data.presenter_email = presenterEmail;
        data.company_name = companyName;
        data.synopsis = synopsis;
        data.title = title;
        data.conference = conference;
        console.log(data);
      
        const presentationUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
      
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);
        
            setPresenterName('');
            setPresenterEmail('');
            setCompanyName('');
            setSynopsis('');
            setTitle('');
            setConference('');
          }
      }

    
    const handlePresenterNameChange = (event) => {
        const value = event.target.value;
        setPresenterName(value);
    }
    const handlePresenterEmailChange = (event) => {
        const value = event.target.value;
        setPresenterEmail(value);
    }

    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }

    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }

    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    const fetchData = async () => {

        const conferencesUrl = 'http://localhost:8000/api/conferences/';
        
        const conferencesResponse = await fetch(conferencesUrl);
    
        if (conferencesResponse.ok) {
            const conferencesData = await conferencesResponse.json();
            setConferences(conferencesData.conferences)
        }
    }

    useEffect(() => {
        fetchData();
      }, []);

    return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input value={presenterName} onChange={handlePresenterNameChange} placeholder="presenter_name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter Name</label>
              </div>
              <div className="mb-3">
                <label htmlFor="presenter_email" className="form-label">Presenter Email</label>
                <input value={presenterEmail} onChange={handlePresenterEmailChange} type="email" className="form-control" id="presenter_email" placeholder="name@example.com"/>
              </div>
              <div className="form-floating mb-3">
                <input value={companyName} onChange={handleCompanyNameChange} placeholder="company_name" required type="text" name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company Name</label>
              </div>
                <div className="form-floating mb-3">
                    <input value={title} onChange={handleTitleChange} placeholder="title" required type="text" name="title" id="title" className="form-control"/>
                    <label htmlFor="title">Title</label>
                  </div>
                  <div className="mb-3">
                <label htmlFor="synopsis" className="form-label">Synopsis</label>
                <textarea value={synopsis} onChange={handleSynopsisChange} className="form-control" id="synopsis" rows="5"></textarea>
            </div>
            <div className="mb-3">
                <select value={conference} onChange={handleConferenceChange} required name="conference" id="conference" className="form-select">
                  <option selected value="">Choose a conference</option>
                  {conferences.map(conference => {
                        return (
                            <option key={conference.id} value={conference.id}>
                            {conference.name}
                            </option>
                        );
                    })}
                </select>
              </div>
              <div>
                <button className="btn btn-primary">Create</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    );
}

export default PresentationForm;
