import React, { useEffect, useState } from 'react';

function AttendeeForm(props) {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('')
    const [conferences, setConferences] = useState([]);
    const [conference, setConference] = useState('')

    const handleSubmit = async (event) => {
        event.preventDefault();
      
        const data = {};
        data.name = name;
        data.email = email;
        data.conference = conference;
        console.log(data);
      
        const conferenceUrl = `http://localhost:8001/api/conferences/${data.conference}/attendees/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
      
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newAttendee = await response.json();
            console.log(newAttendee);
        
            setName('');
            setEmail('');
            setConference('');
          }
      }

    
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    
    const fetchData = async () => {

        const conferencesUrl = 'http://localhost:8000/api/conferences/';
        
        const conferencesResponse = await fetch(conferencesUrl);
    
        if (conferencesResponse.ok) {
            const conferencesData = await conferencesResponse.json();
            setConferences(conferencesData.conferences)
        }
    }

    useEffect(() => {
        fetchData();
      }, []);

      return (
        <div className="my-5 container">
        <div className="my-5 container">
          <div className="row">
            <div className="col col-sm-auto">
              <img width="300" alt="" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg"/>
            </div>
            <div className="col">
              <div className="card shadow">
                <div className="card-body">
                  <form onSubmit={handleSubmit} id="create-attendee-form">
                    <h1 className="card-title">It's Conference Time!</h1>
                    <p className="mb-3">
                      Please choose which conference
                      you'd like to attend.
                    </p>
                    <div className="mb-3">
                      <select value={conference} onChange={handleConferenceChange} name="conference" id="conference" className="form-select" required>
                        <option value="">Choose a conference</option>
                        {conferences.map(conference => {
                        return (
                            <option key={conference.id} value={conference.id}>
                            {conference.name}
                        </option>
                        );
                    })}
                      </select>
                    </div>
                    <p className="mb-3">
                      Now, tell us about yourself.
                    </p>
                    <div className="row">
                      <div className="col">
                        <div className="form-floating mb-3">
                          <input value={name} onChange={handleNameChange} required placeholder="Your full name" type="text" id="name" name="name" className="form-control"/>
                          <label htmlFor="name">Your full name</label>
                        </div>
                      </div>
                      <div className="col">
                        <div className="form-floating mb-3">
                          <input value={email} onChange={handleEmailChange} required placeholder="Your email address" type="email" id="email" name="email" className="form-control"/>
                          <label htmlFor="email">Your email address</label>
                        </div>
                      </div>
                    </div>
                    <button className="btn btn-lg btn-primary">I'm going!</button>
                  </form>
                  <div className="alert alert-success mb-0" id="success-message">
                    Congratulations! You're all signed up!
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

export default AttendeeForm;
