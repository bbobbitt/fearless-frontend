import React, { useEffect, useState } from 'react';

function ConferenceForm(props) {
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setMaxPresentations] = useState('');
    const [maxAttendees, setMaxAttendees] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]);

    const handleSubmit = async (event) => {
        event.preventDefault();
      
        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;
        console.log(data);
      
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
      
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
        
            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');
          }
      }

    
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }

    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleMaxPresentions = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }

    const handleMaxAttendees = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    
    const fetchData = async () => {

        const locationsUrl = 'http://localhost:8000/api/locations/';
        
        const locationsResponse = await fetch(locationsUrl);
    
        if (locationsResponse.ok) {
            const locationsData = await locationsResponse.json();
            setLocations(locationsData.locations)
        }
    }

    useEffect(() => {
        fetchData();
      }, []);

      return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new conference</h1>
              <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                  <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <div>Start date:</div>
                  <input value={starts} onChange={handleStartsChange} type="date" id="starts" name="starts"/>
                </div>
                <div className="form-floating mb-3">
                  <div>End Date:</div>
                  <input value={ends} onChange={handleEndsChange}type="date" id="ends" name="ends"/>
                </div>
                <div className="mb-3">
                  <label htmlFor="description" className="form-label">Description</label>
                  <textarea value={description} onChange={handleDescriptionChange} className="form-control" id="description" name ="description" rows="3"></textarea>
                </div>
                <div className="form-floating mb-3">
                  <input value={maxPresentations} onChange={handleMaxPresentions}placeholder="max presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                  <label htmlFor="max-presentations">Max Presentations</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={maxAttendees} onChange={handleMaxAttendees} placeholder="max attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                  <label htmlFor="max-attendees">Max Attendees</label>
                </div>
                <div className="mb-3">
                  <select value={location} onChange={handleLocationChange} required id="location" name="location" className="form-select">
                    <option value="">Choose a Location</option>
                    {locations.map(location => {
                        return (
                            <option key={location.id} value={location.id}>
                            {location.name}
                            </option>
                        );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }

export default ConferenceForm;
