import Nav from './Nav';
import LocationForm from './LocationForm';
import AttendeesList from './AttendeesList';
import ConferenceForm from './ConferenceForm';
import AttendeeForm from './AttendeeForm'
import PresentationForm from './PresentationForm';
import MainPage from './MainPage'

import { BrowserRouter, Routes, Route } from "react-router-dom";


function App(props) {
  return (
    <>
    <div className="app">
      <BrowserRouter>
      <Nav />
      <Routes>
        <Route path="/conferences/new" element={<ConferenceForm />}></Route>
        <Route path="/attendees/new" element={<AttendeeForm />}></Route>
        <Route path="/locations/new" element={<LocationForm />}></Route>
        <Route path="/attendees/" element={<AttendeesList attendees={props.attendees}/>}></Route>
        <Route path="/locations/new" element={<LocationForm />}></Route>
        <Route path="/presentations/new" element={<PresentationForm />}></Route>
        <Route index element={<MainPage />} />
      </Routes>
      </BrowserRouter>
    </div>
    </>
  );
}

export default App;
